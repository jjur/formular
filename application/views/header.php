<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Zóna jednotkárov</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo base_url();?>assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body class="grey-new ">
<ul id="slide-out" class="side-nav fixed grey-new darken-2">
    <li class="grey-new darken-1">
        <nav class="orange">
            <ul class="hide-on-med-and-down">
                <li><h4 id="logo-container" style="right: 10px" class=" brand-logo white-text">VIP zóna</h4>
                </li>
            </ul>
        </nav>
    </li>
    <li class="grey-new darken-1 white-text"><a class="white-text"><i class="material-icons white-text"></i></a></li>
    <li class="grey-new darken-1 white-text"><a class="white-text"><i class="material-icons white-text">description</i>Informácie</a></li>
    <li class="grey-new darken-1 grey-text text-lighten-1"><a href="https://www.python.org/" class="grey-text text-lighten-1">Python 3</a></li>
    <li class="grey-new darken-1 grey-text text-lighten-1"><a href="https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol" class="grey-text text-lighten-1">HTTP</a></li>
    <li class="grey-new darken-1 grey-text text-lighten-1"><a href="https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_message" class="grey-text text-lighten-1">Requests</a></li>
    <li class="grey-new darken-1 grey-text text-lighten-1"><a href="https://en.wikipedia.org/wiki/Computer_network" class="grey-text text-lighten-1">Network</a></li>
    <li class="grey-new darken-1 white-text"><a class="white-text"><i class="material-icons white-text">assignment_ind</i>Začíname</a></li>
    <li class="grey-new darken-1 grey-text text-lighten-1"><a href="/" class="grey-text text-lighten-1">Prihlásenie</a></li>
    <li class="grey-new darken-1 grey-text text-lighten-1"><a href="/" class="grey-text text-lighten-1">Formulár</a></li>
    <li class="grey-new darken-1 white-text"><a class="white-text"><i class="material-icons white-text"></i></a></li>
</ul>
<main>
    <nav class="white grey-text">
        <ul class="left show-on-medium-and-down">
            <li><a href="#" data-activates="slide-out" class="button-collapse"><i
                            class="material-icons grey-text text-darken-3">menu</i></a></li>
        </ul>
        <ul class="center hide-on-med-and-down">
            <li><a id="logo-container" href="#" style="left: 310px" class="brand-logo grey-text text-darken-3">   Len pre odvážnych</a></li>
        </ul>
        <ul class="center hide-on-med-and-up">
            <li><a id="logo-container" href="#" class="brand-logo grey-text text-darken-3"><b>VIP zóna</a></li>
        </ul>
        <ul class="right hide-on-small-only">
            <li><a class="grey-text text-darken-3" href="<?php echo base_url();?>">Prihlásenie</a></li>
        </ul>
    </nav>