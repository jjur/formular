<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header");
?>
    <div class="container">
        <br><br>
        <div class="row ">
            <div class="col xl1 l2 m3 s3"><i class="large material-icons orange-text">security</i></div>
            <div class="col xl11 l10 m9 s9"><h4 class="white-text">Chránená zóna</h4>
            <p class="grey-text text-lighten-1">Pre prístup do systému sa vyžaduje prihlásenie</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col s12 m6">
                <div class="card medium grey-new lighten-1 ">
                    <div class="card-content grey-text text-lighten-1">
                        <span class="card-title white-text">Prihlásenie</span>
                        <p>I am a very simple card. I am good at containing small bits of information.
                            I am convenient because I require little markup to use effectively.</p>
                        <form method="post" action="<?php echo base_url();?>account/login">
                            <br>
                            <div class="input-field col s12">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="icon_prefix" name="username" type="text" class="">
                                <label for="icon_prefix">Používateľské meno</label>
                            </div>
                            <div class="input-field col s12">
                                <i class="material-icons prefix">lock</i>
                                <input id="icon_prefix" name="password" type="password" class="">
                                <label for="icon_prefix">PIN</label>
                            </div>
<br>
                            <center><button class="btn waves-effect waves-light center orange" type="submit" name="action">Prihlásiť sa
                                <i class="material-icons right">send</i>
                            </button></center>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col s12 m6">
                <div class="card grey-new lighten-1 text-lighten-1 ">
                    <div class="card-content grey-text">
                        <span class="card-title white-text">Prihlasovací formulár</span>
                        <p>I am a very simple card. I am good at containing small bits of information.
                            I am convenient because I require little markup to use effectively.</p>
                    </div>

                </div>
            </div>
        </div



<endora></endora>


    </div>
</main>
</body>

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/materialize.js"></script>
<script src="<?php echo base_url();?>assets/js/init.js"></script>

</html>